
public class Square extends Shape implements TwoDimensionalShapeInterface {

	private double side;
	
	//public Square(int side)
	//{
		
		//this.side=side;
		
	//}
	
	
	
	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double calculateArea() {
		
		double area = side*side;
		System.out.println("The area of Square is" +area);
		return area;
	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		
	}

}
